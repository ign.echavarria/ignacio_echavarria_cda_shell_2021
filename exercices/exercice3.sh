#!/bin/bash
# exit 2 = L'utilisateur n'a pas saissi un nom de fichier.
# exit 3 = le fichier n'existe pas.
# Variable TIME_LINE permets de changer la vitesse pour afficher la ligne suivante.
FICHIER=$1
TIME_LINE=0.5

# La fonction file_exists, controle si l'utilisateur a saissi un nom de fichier et si le fichier existe.
# Si l'utilisateur n'a pas tapé un nom de fichier, le script sort avec le code 2; s'il a tapé un nom de fichier qui n'existe pas, le script sort avec le code 3. 
function file_exists() {
	[ -z $FICHIER ]
	case $? in
		0)
			echo "Il faut saissir un nom de fichier."
			exit 2
			;;
	esac
	[ -f $FICHIER ]
	case $? in
		0)
			;;
		*)
			echo "Le fichier $FICHIER n'existe pas."
			exit 3
			;;
	esac
}

# Fonction read_lines, va lire le fichier saissi par l'utilisateur ligne par ligne, avec un temps de retardement de 0,5s par ligne.
function read_lines() {
	cat $FICHIER | while read ligne
do
	echo "$ligne"
	sleep $TIME_LINE
done
}

file_exists
read_lines

