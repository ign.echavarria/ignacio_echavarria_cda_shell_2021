#!/bin/bash
# ATTENTION, des modifications dans le fichier projet1.sh peuvent interferir avec le fonctionnement de ce script.
# exit 2 = L'utilisateur n'a pas fourni une option valide.
# exit 3 = L'utilisateur n'a pas fourni un nom d'utilisateur.
# exit 4 = L'utilisateur a essayé de supprimer un utilisateur qui n'existe pas.
# exit 5 = L'utilisateur a essayé de créer un utilisateur qui est déjà existant.

USER=""
EXISTE=false
OPTION=""
VERIFY_USER="./projet1.sh"

# La fonction select_option permets à l'utilisateur de choisir entre les options. Si l'utilisateur n'a rien choisi ou si l'option choisi par l'utilisateur n'est pas entre les proposées, le programme sort avec le code 2.
select_option() {
	echo -e "Choisissez l'option:\n1 - Supprimer un utilisateur.\n2 - Créer un utilisateur."
	read OPTION
	case "$OPTION" in
		"") 
			echo "Il faut choisir une option."
			exit 2
			;;
		1|2)
			;;
		*)
			echo "Désolé, l'option $OPTION n'est pas valide."
			exit 2
			;;
	esac
}

# La fonction user_name permets au utilisateur de saissir un nom d'utilisateur. Il verifie si l'utilisateur a saissi un nom, et s'il n'a rien saissi, le script sort avec le code 3
user_name() {
	echo "Saisissez le nom d'utilisateur."
        read USER
	if [ -z $USER ]
	then
		exit 3
	fi

}

# La fonction suppression utilise le script du projet 1 pour verifier l'existence de l'utilisateur saissi. S'il existe, il procede à le supprimmer, sinon, il affiche un message et le programme sort avec le code 4.
suppression() {
	    $VERIFY_USER $USER > /dev/null
	    case "$?" in
        	3)
                	echo "L'utilisateur $USER n'existe pas, vous ne pouvez pas le supprimer."
			exit 4
                	;;
                0)
                        sudo userdel $USER
                        echo "Utilisatuer $USER supprimé."
                        ;;
	esac
}

# La fonction creation utilise le script du projet 1 pour verifier l'existence de l'utilisateur saissi. S'il n'existe, il procede à le créer, sinon, il affiche un message et le programme sort avec le code 5.
creation() {
	$VERIFY_USER $USER > /dev/null
	case "$?" in
        	3)
			sudo adduser $USER
                        echo "Utilisateur $USER crée."
                        ;;
                0)	
	         	echo "Il existe déjà l'utilisateur $USER, vous ne pouvez pas le créer."
                        exit 5
			;;
	esac
}



select_option
case $OPTION in
	1)
		user_name
		suppression
		;;
	2)
		user_name
		creation
		;;
esac
