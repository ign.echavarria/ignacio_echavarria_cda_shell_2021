#!/bin/bash
# exit 2 = L'utilisateur n'a pas transmis un entier positif comme paramètre.

NUM=$1
FACTEUR=1

# La fonction check_integer verifie si l'utilisateur a transmis un entier positif comme paramètre, si ce n'est pas le cas, le script sort avec le code 22
check_integer() {
	[[ $NUM =~ ^[0-9]+$ ]]
	case $? in
		0)
			;;
		*)
			echo "Il faut transmettre un entier positif  comme paramètre."
			exit 2
			;;
	esac
}

# La fonction factorielle va calculer et afficher le factorielle du numero que l'utilisateur a transmis comme paramètre.
factorielle() {
	if [ $NUM -gt 1 ]
        then
                FACTEUR=$(($FACTEUR * $NUM))
                ((NUM--))
                factorielle $NUM            
        else
                echo "$FACTEUR"
        fi
}

check_integer
factorielle
