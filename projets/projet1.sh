#!/bin/bash
# ATTENTION, des modifications sur ce fichier peuvent interferir avec l'execution des autres projets (projets 3 et 6)
# exit 2 = L'utilisateur n'a pas saissi aucun parametre.
# exit 3 = L'utilisateur a saissi un nom d'utilisateur qui n'existe pas.

USER=$1

# La fonction check_input verifie si l'utilisateur a saissi un paramètre. Sinon, le programme sort avec le code 2.
check_input() {
	if [ -z $USER ]
	then
		echo "Il faut saissir un nom d'utilisateur."
		exit 2
	fi
}
# La fonction check_user verifies si l'utilisateur existe ou n'existe pas. S'il n'existe pas, le script va sortir avec le code 3
check_user() {
	IN_LIST=$(cut -d ":" -f 1 /etc/passwd | grep -x $USER)
	[ "$IN_LIST" == $USER ]
	case "$?" in
        0)
        	echo "L'utilisateur $USER existe."
	        ;;    
	*)
		echo "L'utilisateur $USER n'existe pas."
		exit 3
        	;;
	esac
}

check_input
check_user
