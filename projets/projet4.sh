#!/bin/bash
# ATTENTION, des modifications dans le script projet2.sh peuvent interferir avec le fonctionnement de ce script.
# exit 2 = L'utilisateur n'a pas choisi une option ou l'option n'est pas valide.
# exit 3 = L'utilisateur n'a pas saissi un nom de groupe.
# exit 4 = L'utilisateur a essayé de supprimer un groupe inexistant.
# exit 5 = L'utilisateur a essayé de créer un groupe déjà existant.
GROUP=""
OPTION=""
VERIFY_GROUP="./projet2.sh"

# La fonction select_option permets à l'utilisateur de choisir entre les options. Si l'utilisateur n'a rien saissi ou si l'option choisi par l'utilisateur n'est pas entre les proposées, le programme sort avec le code 2.
select_option() {
	echo -e "Choisissez l'option:\n1 - Supprimer un groupe.\n2 - Créer un groupe."
	read OPTION
	case "$OPTION" in
		"")
			echo "Il faut choissir une option."
			exit 2
			;;
		1|2)
			;;
		*)
			echo "$OPTION n'est pas une option valide."
			exit 2
			;;
	esac
}

# La fonction group_name permets au utilisateur de saissir un nom de groupe. Il verifie si l'utilisateur a saissi un nom, et s'il n'a rien saissi, le script sort avec le code 3
group_name() {
	echo "Saisissez le nom du groupe."
        read GROUP
	if [ -z $GROUP ]
	then
		echo "Il faut saissir un groupe."
		exit 3
	fi

}

# La fonction suppression utilise le script du projet 2 pour verifier l'existence du groupe saissi. S'il existe, il procede à le supprimmer, sinon, il affiche un message et le programme sort avec le code 4.
suppression() {
	$VERIFY_GROUP $GROUP > /dev/null
	case "$?" in
        	3)
                	echo "Le groupe $GROUP n'existe pas, vous ne pouvez pas le supprimer."
			exit 4
                	;;
                0)
                        sudo groupdel $GROUP
                        echo "Groupe $GROUP supprimé."
                        ;;
	esac
}

# La fonction creation utilise le script du projet 2 pour verifier l'existence du groupe saissi. S'il n'existe, il procede à le créer, sinon, il affiche un message et le programme sort avec le code 5.
creation() {
	$VERIFY_GROUP $GROUP > /dev/null	
	case "$?" in
        	3)
			sudo addgroup $GROUP
                        echo "Groupe $GROUP crée."
                        ;;
                0)	
	         	echo "Il existe déjà le groupe $GROUP, vous ne pouvez pas le créer."
                        exit 5
			;;
	esac
}



select_option
case $OPTION in
	1)
		group_name
		suppression
		;;
	2)
		group_name
		creation
		;;
esac
