#!/bin/bash
#exit 2 = Il y'a pas la quantité de parametres requis
#exit 3 = Les parametres ne sont pas tous des nombres entieres

PARAMETRES_TO_GIVE=10
PARAMETRES=$@
ARRAY=()
QTY_PARAMETRES=$#
RE='^[-]?[0-9]+$'
INDICE=0

# La fonction create_array crée et affiche un tableau avec les paramétres donnés par l'utilisateur.
create_array() {
	for PARAMETRE in $PARAMETRES
	do
		ARRAY[$INDICE]=$PARAMETRE
		((INDICE++))
	done
	echo "Votre tableau est: ${ARRAY[*]}"
}

# La fonction check_param controle que l'utilisateur a saissi la quantité des paramétres requis par le script et s'ils sont des entiers.
# Si l'utilisateur n'a pas renseigné la quantité nécessaire de paramétres, le script va sortir avec le code 2.
# Si tous les parametres renseignés par l'utilisateur ne sont pas des nombres entiers, le script va sortir avec le code 3.
check_param() {
	if [ $QTY_PARAMETRES -ne $PARAMETRES_TO_GIVE ]
	then
		echo "Il faut renseigner $PARAMETRES_TO_GIVE nombres entieres"
		exit 2
	else
		for PARAMETRE in $PARAMETRES
		do
			[[ $PARAMETRE =~ $RE ]]
			case $? in
				0)
					;;
				*)
					echo "Tous les parametres doivent etre des nombres entiers"
					exit 3
					;;
			esac
		done	
	fi
}

# La fonction bubble_sort va trier les parametres dans l'ordre croissant et va les afficher. Cette fonction s'execute en boucle jusqu'a que les parametres soient ordonnés. 
bubble_sort() {
	I=$((PARAMETRES_TO_GIVE-1))
	PERMUTATION=1
	while [[ $I -gt 0 && $PERMUTATION -eq 1 ]]
	do
		PERMUTATION=0
		J=0
		while [ $J -lt $I ]
		do
			[[ ${ARRAY[$J]} -gt ${ARRAY[$J+1]} ]]
			case $? in
				0)
					TMP=${ARRAY[$J]}
					ARRAY[$J]=${ARRAY[$J+1]}
					ARRAY[$J+1]=$TMP
					PERMUTATION=1
					;;
			esac
			((J++))
		done
		((I--))
	done
	echo "Votre nouveau tableau est ${ARRAY[*]}"	
}	

check_param
create_array
bubble_sort

