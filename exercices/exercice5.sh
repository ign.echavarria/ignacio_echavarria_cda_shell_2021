#!/bin/bash
# Le script verifie si l'utilisateur passé comme paramtre n'est pas connecté. Si l'utilisateur est connecté, le script n'affiche rien.
# Il y'a un bug dans le script: dans le cas ou un nom d'utilisateur est contenu dans le resultat de la commande who, il n'affiche rien, meme si l'utilisateur n'est pas connecté."
# Par exemple, j'ai crée l'utilisateur "nacio" et si je suis connecté avec l'utilisateur "ignacio", le script  n'affiche pas que l'utilisateur "nacio" n'est pas connecté.

w=`who | grep $1`
if [ -z "$w" ]; then echo "$1 n'est pas connecté.";
fi
