#!/bin/bash

USER=""
USERID=""

# La fonction user permets a l'utilisateur de saisir le nom d'utilisateur desiré.
user() {
	read -p "Saisissez le nom d'utilisateur: " USER
}

# La fonction check_user controle si l'utilisateur a saissi un utilisateur qui existe ou que n'existe pas.
check_user() {
	USERID=$(id $USER 2> /dev/null)
	case $? in 
		0)
			echo "L'utilisateur $USER existe."
			;;
		*)
			echo "L'utilisateur $USER n'existe pas."
			;;
	esac	
}

# La fonction user_id affiche le id de l'utilisateur saissi.
user_id() {
	id -u $USER 2> /dev/null
}

# La fonction menu affiche un menu avec les options disponibles pour l'utilisateur. Il s'affiche jusqu'a que l'utilisateur utilise l'option "Q".
menu() {
	while [ "$SELECTION" != "Q" ]
	do
		echo -e "Saisissez l'option desirée:\n1 - Vérifier l'existence d'un utilisateur\n2 - Connaître l'uid d'un utilisateur\nq - quitter"
		read SELECTION
		case "$SELECTION" in
		1)
			user
			check_user
			;;
		2)
			user
			check_user
			user_id
			;;
		Q|q)
			SELECTION="Q"
			;;
		*)
			echo "L'option choisi n'est pas valide."
			;;
		esac
	done
}

menu
