#!/bin/bash

# La fonction question affiche un menu pour choisir une option jusqu'a que l'utilisateur choisisse l'option "Q".
question() {
	while [ true ]
		do
			echo -e "1 - Windows?\n2 - MacOS?\n3- Linux?\n4 - Unix?\n9 - Quitter?"
			read ANSWER
			case "$ANSWER" in
				1)
					echo "Dommage!"
					;;
				2)
					echo "Peut mieux faire!"
					;;
				3)	
					echo "Pas mal!"
					;;
				4)
					echo "Super!"
					;;
				9)
					exit
					;;
				*)
					echo "$ANSWER n'est pas une option valide".
					;;
			esac
		done

}

question
