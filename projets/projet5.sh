#!/bin/bash
# exit 2 = L'utilisateur n'a pas saissi une option ou il a saissi une option non valide.
# exit 3 = L'utilisateur n'a pas saissi un nom de fichier.
# exit 4 = L'utilisateur a essayé de supprimer un fichier qui n'existe pas.
# exit 5 = L'utilisateur a essayé de créer un fichier déjà existent.

FICHIER=""
OPTION=""
CONTENU=$(ls)

# La fonction select_option permets au utilisateur de choisir entre les options pour supprimer ou créer un fichier. Si l'utilisateur ne tape aucune option un tape une option non-valide, le script sort avec le code 2
select_option() {
	echo -e ."Choisissez l'option:\n1 - Supprimer un fichier.\n2 - Créer un fichier."
	read OPTION
	case "$OPTION" in
		"")
			echo "Vous n'avez pas choisi une option."
			exit 2
			;;
		1|2)
			;;
		*)
			echo "$OPTION n'est pas une option valide."
			exit 2
			;;
	esac
}

# La fonction nom_fichier permets à l'utilisateur de saissir un nom de fichier. si l'utilisateur ne tape rien, il envoie un message et le script sort avec le code 3.
nom_fichier() {
	echo "Saisissez le nom du fichier."
        read FICHIER
	if [ -z $FICHIER ]
	then
		echo "Vous n'avez pas saissi un nom de fichier."
		exit 3
	fi

}
#
suppression() {
	[ -f $FICHIER ]
	case "$?" in
        	0)
                	rm $FICHIER
			case "$?" in
				0)
					echo "Le fichier $FICHIER a été suprimé."
					;;
				*)
					echo "Une erreur s'est produit lors de la suppresion du fichier $FICHIER."
					;;
			esac
	          	;;
                *)
                        echo "Le fichier $FICHIER ne peut pas etre supprimé, il n'existe pas."
                        exit 5
			;;
	esac
}

creation() {
	[ -f $FICHIER ]
	case "$?" in
        	0)
			echo "Le fichier $FICHIER existe déjà, vous ne pouvez pas le créer."
                        exit 6
			;;
                *)	
			touch $FICHIER
			case "$?" in
                                0)
                                        echo "Le fichier $FICHIER a été créé."
                                        ;;
                                *)
                                        echo "Une erreur s'est produit lors de la creation du fichier $FICHIER."
                                        ;;
			esac
			;;
	esac
}



select_option
case $OPTION in
	1)
		nom_fichier
		suppression
		;;
	2)
		nom_fichier
		creation
		;;
esac
