#!/bin/bash
# exit 1 = L'utilisateur n'a pas saissi le nom du fichier.
# exit 2 = L'utilisateur a saissi un fichier qui n'existe pas.
# exit 3 = L'utilisateur a choisi un utilisateur qui n'existe pas.
# exit 4 = L'utilisateur a choisi un groupe qui n'existe pas.
# exit 5 = Erreur d'execution de commande chown ou chgrp

FICHIER=""
OPTION=""
GROUP=""
USER=""

nom_fichier() {
	echo "Saisissez le nom du fichier."
        read FICHIER
	if [ -z $FICHIER ]
	then
		echo "Il est nécessaire de saissir le nom d'un fichier."
		exit 1
	fi

}

existance_fichier() {
	if [ ! -f $FICHIER ]
		then
			echo "Le fichier $FICHIER n'existe pas."
			exit 2
	fi
}

modification_utilisateur() {
	echo "Saisissez le nouveau utilisateur propiétaire."
	read USER
	./projet1.sh $USER > /dev/null
	case "$?" in
		3)
			echo "L'utilisateur $USER n'existe pas, il peut pas etre assigné comme propiétaire."
			exit 3
			;;
		0)
			sudo chown $USER $FICHIER
			case "$?" in
				0)
					echo "$USER a desormais les droits de propiétaire du fichier $FICHIER."
					;;
				*)
					echo "Une erreur s'est produit lors du changement des droits de propiétaire du fichier $FICHIER."
					exit 5
					;;
			esac
			;;
	esac

}

modification_groupe() {
	   echo "Saisissez le nouveau groupe propiétaire."
        read GROUPE
        ./projet2.sh $GROUPE > /dev/null
        case "$?" in
                3)
                        echo "Le groupe $GROUPE n'existe pas, il peut pas etre assigné comme propiétaire."
                        exit 4
			;;
                0)
                        sudo chgrp $GROUPE $FICHIER
                        case "$?" in
                                0)
                                        echo "$GROUPE a desormais les droits de propiétaire du fichier $FICHIER."
                                        ;;
                                *)
                                        echo "Une erreur s'est produit lors du changement des droits du groupe propiétaire du fichier $FICHIER."
                                        exit 5
					;;
                        esac
                        ;;
        esac
}

nom_fichier
existance_fichier
case $? in
	0)
		modification_utilisateur
		modification_groupe
		;;
esac
