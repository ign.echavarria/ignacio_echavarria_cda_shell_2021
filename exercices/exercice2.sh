#!/bin/bash
# exit 2 = L'utilisateur n'a pas saissi des paramètres.

# Fonction check_parametres affiche un message si l'utilisateur n'a pas saissi des paramètres.
no_arguments() {
	echo "Sans arguments."
}

# Fonction show_parametres affiche les paramètres saissis par l'utilisateur.
show_arguments() {
	while [ $# -gt 0 ]
	do
		echo "$1"
		shift
	done
}

[ $# -eq 0 ]
case $? in
	0)
		no_arguments
        	;;
	*)
		show_arguments $@
		;;
esac
