#!/bin/bash
#exit 2 = Il n'y a pas de sous-répertoires dans le répertoire actuel.

CURRENT_DIRECTORY=$(pwd)
DIRECTORIES=0

# La fonction show_directories va parcourir et afficher tous les sous-répertoires dans le répertoire actuel.
show_directories() {
	for FICHIER in $LIST
        do
                if [ -d $FICHIER ]
                then
                        echo "$FICHIER"
			((DIRECTORIES++))
                fi
        done
}

# La fonction no_directories affiche un message si le répertoire actuel n'a pas de sous-répertoires et le script sort avec le code 2.
no_directories() {
	LIST=$(ls -A)
	show_directories
	if [ $DIRECTORIES -eq 0 ]
	then
		echo "Il n'y a pas de sous-répertoires dans $CURRENT_DIRECTORY"
		exit 2
	fi
}

no_directories
