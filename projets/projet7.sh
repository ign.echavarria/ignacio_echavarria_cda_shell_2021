#!/bin/bash
#exit 1 = Il manque le nom du fichier
#exit 2 = le fichier saissi n'existe pas
#exit 3 = Saissi des droites pas aceptée
#exit 4 = Les modifications ne sont pas réalisées

FICHIER=""
PROPIETAIRE="propietaire"
GROUPE="groupe"
AUTRES="autres"
UTILISATEURS=($PROPIETAIRE $GROUPE $AUTRES)

nom_fichier() {
	echo "Saisissez le nom du fichier."
        read FICHIER
	if [ -z $FICHIER ]
	then
		echo "Il est nécessaire de saissir le nom d'un fichier."
		exit 1
	fi

}

existance_fichier() {
	[ -f $FICHIER ]
	case "$?" in
        	0)
                	return 0
			;;
		*)
			echo "Le fichier $FICHIER n'existe pas."
			exit 2
			;;
	esac
}

rights_selection() {
	echo "Saisissez les nouveaus droits pour $1."
	read  REPONSE
	case "$REPONSE" in
		0|---)
			REPONSE="0"
			;;
		1|--X|--x|x|X)
			REPONSE="1"
			;;
		2|-W-|-w-|w|W)
			REPONSE="2"
			;;
		3|-WX|-wx|wx|WX|XW|xw)
			REPONSE="3"
			;;
		4|R--|R--|r|R)
			REPONSE="4"
			;;
		5|R-X|r-x|rx|RX|XR|xr)
			REPONSE="5"
			;;
		6|RW-|rw-|RW|rw|WR|wr)
			REPONSE="6"
			;;
		7|RWX|rwx|RXW|rxw|WXR|wxr|WRX|wrx|XRW|xrw|XWR|xwr)
			REPONSE="7"
			;;
		*)
			echo "Les droites saissis ne sont pas aceptés."
			rights_selection $1
	esac

}

function rights_modification() {
	for ITEM in $PROPIETAIRE $GROUPE $AUTRES
	do
		rights_selection $ITEM
		RIGHTS=${RIGHTS}$REPONSE
	done
	chmod $RIGHTS $FICHIER
}

nom_fichier
existance_fichier
rights_modification
case $? in
	0)
		LRIGHTS=$(ls -al $FICHIER | cut -c1-10)
		echo "Les droits du fichier $FICHIER ont changé: $RIGHTS (${LRIGHTS})"
		;;
	*)	
		echo "Une erreur c'est produit lors du changement des droits du $FICHIER"
		exit 4
		;;
esac

