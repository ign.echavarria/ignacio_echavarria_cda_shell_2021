#!/bin/bash
# exit 2 = L'utilisateur n'a pas saissi le parametre requis.
# exit 3 = Le parametre n'est pas un entier positif.
LAST=0
NUMBER=$1

# La fonction check_input verifie si l'utilisateur a saissi el paramètre requis et s'il est majeur a 0.
# Si l'utilisateur n'a pas tapé le paramètre, le script sort avec le code 2. si le paramètre n'est pas un entier majeur a 0, le script sort avec le code 3.
check_input() {
	[ -z $NUMBER ]
	case $? in
		0)
			echo "L'utilisateur n'a pas saissi des paramètres."
			exit 2
			;;
	esac
	if ! [ $NUMBER -gt 0 ] 2> /dev/null
	then
		echo "Le parametre doit etre un entier majeur a 0."
		exit 3
	fi
}

# La fonction decrease_to_1 affiche tous les numeros entre le numero saissi et le 1 (incluant les deux)
decrease_to_1() {
	while [ $NUMBER -gt $LAST ]
	do
		echo "$NUMBER"
		NUMBER=$(($NUMBER - 1))
	done
}

check_input
decrease_to_1
