#!/bin/bash
# exit 2 = Mauvaise format de date ou date n'existe pas
# exit 3 = Nombre des cadeaux n'est pas un entier positif ou 0

# read -p "Saisissez votre date de naissance (JJ/MM/AAAA): " NAISSANCE
# read -p "Combien de cadeaux avez vous reçu? " CADEAUX

# La fonction change_format modifie le format de la date saissie par l'utilisateur au format AAAA-MM-JJ
function change_format() {
	ANNEE=$(echo $NAISSANCE | cut -d "/" -f 3)
	MOIS=$(echo $NAISSANCE | cut -d "/" -f 2)
	JOUR=$(echo $NAISSANCE | cut -d "/" -f 1)
	DATE="${ANNEE}-${MOIS}-${JOUR}"
}


# La fonction ne_le_1  utilise la date de naissance saissi par l'utilisateur, pour verifier si elle est bien le 01/01/1981
function ne_le_1() {
	[ $NAISSANCE == "01/01/1981" ]
	case "$?" in
		0)
			echo "Vous etes né le 1er janvier 1981."
			;;
		*)
			echo "Vous n'etes pas né le 1er janvier 1981."
			;;
	esac
}

# La fonction cinq_cadeaux verifie si l'utilisateur a saissi s'il a reçu cinq ou plus cadeaux.
function cinq_cadeaux() {
	[ $CADEAUX -ge 5 ]
	case "$?" in
		0)
			echo "Vous avez reçu 5 cadeaux ou plus!"
			;;
		*)
			echo "Vous n'avez pas reçu 5 cadeaux."
			;;
	esac
}

#la fonction jour_de_naissance verifie si la date de naissance saissi par l'utilisateur était un samedi.
function jour_de_naissance() {
	case "`date -d $DATE +%A`" in
		Saturday)
			echo "Vous etes né un samedi."
			;;
		*)
			echo "Vous n'etes pas né un samedi."
			;;
	esac
}

# la fonction check_date verifies le format et l'existance de la date saissi par l'utilisateur et elle est une date future.
# Si l'utilisateur tape une date sans le format requis ou une date inexistente, le script sort avec le code 2.
function check_date() {
	if [ ! "`date '+%Y-%m-%d' -d $DATE 2> /dev/null`" == "$DATE" ]
	then
		echo "Mauvaise format de date ou date inexistente."
		exit 2
	fi
}

# La fonction check_gifts verifie que l'utilisateur a tapé un quantité de cadeaux valide (entier majeur ou égal a 0), sinon le script sort avec le code 3.
function check_gifts() {
	[[ $CADEAUX =~ ^[0-9]+$ ]]
#	[ $CADEAUX -ge 0 ]
	case $? in
		0)
			;;
		*)
			echo "Le nombre de cadeaux doit etre un entier majeur ou égal a 0."
			exit 3
			;;
	esac
}

read -p "Saisissez votre date de naissance (JJ/MM/AAAA): " NAISSANCE
change_format
check_date
read -p "Combien de cadeaux avez vous reçu? " CADEAUX
check_gifts
ne_le_1
cinq_cadeaux
jour_de_naissance
