Travaux pratiques notés
Script Shell

### Exercices ###

# Exercice 1
Le programme demande a l'utilisateur de renseigner sa date de naissance et la quantité de cadeaux qu'il a reçu. Après avoir controlé le format de la date et s'il s'agit d'une date valide et si le nombre de cadeaux est un entier positif, et par l'utilisation de la commande echo, le script va afficher des messages affichant si l'utilisateur est né le 01/01/1981 ou pas; s'il a reçu un minimum de 5 cadeaux ou pas. et s'il a été né un samedi ou pas.

# Exercice 2
Le programme affiche tous les arguments donnés par l'utilisateur, en ordre d'apparition. S'il n'y a pas d'arguments, il affiche le message "Sans arguments".

# Exercice 3
Le programme affiche ligne par ligne le contenu du fichier donné comme paramètre par l'utilisateur. S'il n'a pas donné de paramètres ou le parametre corresponde pas à un fichier, le script envoie un message.

# Exercice 4
Le programme de l'exercice 4 affiche tous les sous-répertoires du répertoire courant. S'il n'y a pas de sous-répertoires, il affiche un message.

# Exercice 5
Le programme affiche un message indiquant si l'utilisateur passé comme paramètre n'est pas online.

# Exercice 6
Le programme affiche la chiffre rentré par l'utilisateur et descend pour afficher tous les chiffres jusqu'a 1. Si l'utilisateur n'a pas saissi des paramètres ou si le paramètre n'est pas un entier positif, le script affiche un message.

# Exercice 7
Le programme affiche en continu un menu avec cinq options, ou l'utilisateur doit saissir une et le programme donnera un message comme réponse. Si l'option saissi n'est pas entre les proposées, le script affiche un message en l'indiquant. Pour sortir du programme, l'utilisateur doit saissir l'option 9.

# Exercice 8
Le programme utilise la technique du "Tri à bulle" pour ordonner des entiérs passés comme paramètres par l'utilisateur. Il va controler que l'utilisateur a passé la quantité correcte de paramètres et qu'il s'agit bien des entiers. Après les verifications, le programme va executer la fonction qui réalise le tri jusqu'a obtenir le tableau ordonné et finalement il l'affiche.

# Exercice 9
Le programme doit recevoir un paramètre. Il envoie un message s'il n'y a pas de paramètre ou s'il ne s'agit pas d'un entier positif. Après les verifications, il procéde a réaliser la fonction factorielle, qui s'agit d'une fonction récursive. Finalement, il affiche le résultat de cette opération.

# Exercice 10
Le programme affiche en continu un menu ou l'utilisateur peut choisir entre 3 options. La prémiere est pour vérifier l'existence d'un utilisateur.Il demande à l'utilisateur de saissir un nom d'utilisateur et il affiche un message disant si l'utilisateur existe ou n'existe pas. La déuxieme est pour connaître l'UID d'un utilisateur, il demande à l'utilisateur de saissir un nom d'utilisateur, il verifie s'il existe et dans le cas ou il existe, il envoi un message affichant son UID. S'il n'existe pas, il envoie un message en l'indiquant. La troisiéme option est pour quitter le menu. Si l'utilisateur tape une option non valide, le programme affiche un message.
