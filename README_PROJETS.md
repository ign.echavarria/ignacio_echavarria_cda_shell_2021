Travaux pratiques notés
Script Shell

### Projets ###

# Projet 1
L'utilisateur doit passer un nom d'utilisateur comme paramètre et le programme le dira si l'utilisateur existe, n'existe pas ou si l'utilisateur n'a rien passé comme paramètre.

# Projet 2
L'utilisateur doit passer un nom de groupe comme paramètre et le programme le dira si le groupe existe, n'existe pas ou si l'utilisateur n'a rien passé comme paramètre.

# Projet 3
Le programme affiche un menu à l'utilisateur, le proposant choisir entre deux options: créer ou supprimer un utilisateur. Une fois l'option  choisi, le utilisateur doit saissir le nom d'utilisateur et le programme verifie si l'utilisateur existe et, s'il est possible, supprime ou crée l'utilisateur selon l'option choisi.

# Projet 4
Le programme affiche un menu à l'utilisateur, le proposant choisir entre deux options: créer ou supprimer un groupe. Une fois l'option  choisi, le utilisateur doit saissir le nom du groupe et le programme verifie si le groupe existe et, s'il est possible, supprime ou crée le groupe selon l'option choisi.

# Projet 5
Le programme affiche un menu proposant à l'utilisateur de choisir entre deux options: créer ou supprime un fichier. Une fois l'option choisi, l'utilisateur doit choisir le nom du fichier. Si l'utilisateur a choisi l'option de supprimer le fichier, le programme verifie si le fichier existe et, s'il existe, le supprime; s'il n'existe pas, il affiche un message en l'indiquant. Si l'utilistauer a choisi l'option de créer le fichier, le programme verifie si le fichier existe. S'il existe, le programme affice un message en l'indiquant; s'il n'existe pas, le programme va le créer.

# Projet 6
Le programme demande à l'utilisateur de saissir le nom d'un fichier, si le fichier n'existe pas, il va afficher un message en l'indiquant; si le fichier existe, il demande à l'utilisateur de saissir le nom du nouvel utilisateur propiétaire; si le nom d'utilisateur saissi n'existe pas, le programme affiche un message en l'indiquant; s'il existe, le programme va changer les droits pour l'utilisateur propiétaire et demander de saissir le nom du nouveau groupe propiétaire. Si le groupe n'existe pas, le programme affiche un message en l'indiquant; s'il existe, le programme va changer les droits pour le groupe propiétaire.

# Projet 7
Le programme demande à l'utilisateur de saissir le nom d'un fichier. Si le fichier n'existe pas, il va afficher un message en l'indiquant. si le fichier existe, il demande à l'utilisateur de saissir les nouevaux droits sur le fichier pour le propiétaire, pour le groupe propiétaire et pour les autres utilisateurs. Finalement, si l'utilisateur a saissi des valeurs valides, le programme procede a modifier les droits.

