#!/bin/bash
#ATTENTION: Des modifications dans ce fichier peuvent modifier le comportement des projets 4 et 6
# exit 2 = L'utilisateur n'a pas saissi un nom de groupe.
# exit 3 = L'utilisateur a saissi le nom d'un groupe qui n'existe pas.

GROUP=$1

# La fonction check_input verifie si l'utilisateur a saissi un paramètre. Sinon, le programme sort avec le code 2.
check_input() {
    if [ -z $GROUP ]
    then
	    echo "Il faut saissir un nom de groupe"
	    exit 2
    fi
}

# La fonction check_group verifies si l'utilisateur existe ou n'existe pas. S'il n'existe pas, le script va sortir avec le code 3
check_group() {
	IN_LIST=$(cut -d ":" -f 1 /etc/group | grep -x $GROUP)
	[ "$IN_LIST" == $GROUP ]
	case "$?" in
       	0)
       		echo "Le groupe $GROUP existe."
       		;;    
	*)
		echo "Le groupe $GROUP n'existe pas."
		exit 3
        	;;
	esac
}

check_input
check_group
